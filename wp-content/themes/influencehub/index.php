<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header' ) ); ?>
<div class="show-bar effeckt-button">
            +
        </div>
        <p class="opener">Welcome to the Influence Hub, Diageo’s very own source of all things creative in the advertising and marketing industry. Here you’ll find everything from clever uses of technology to the greatest on-trade activations, and quirky use of media spaces to innovative digital campaigns. No need to trawl the Internet, the good stuff is all here.</p>
		<?php if ( have_posts() ): ?>
        <div class="blogTable">
        <div class="gutter-sizer"></div>
        <div class="grid-sizer"></div>
            <ul class="blogList unstyled clearfix">

		<?php while ( have_posts() ) : the_post(); ?>
                <li class="blogList-item blogList-large">
                    <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                        <figure class="effeckt-caption">
                            <?php
                                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'your_thumb_handle' ); ?>
<img src="<?php echo $thumbnail['0']; ?>" />

                            <figcaption>
                                <div class="effeckt-figcaption-wrap">
                                    <h3><?php the_title(); ?></h3>
                                </div>
                            </figcaption>
                        </figure>
                    </a>
                </li>
		<?php endwhile; ?>
		</ul>
		<?php else: ?>
		<h2>No posts to display</h2>
		<?php endif; ?>
        </div>
        <?php if ($wp_query->max_num_pages > 1) : ?>
            <footer class="footer" role="contentinfo">
                <p class="new"><?php previous_posts_link(__( '&gt;&gt;', 'roots' )); ?></p>
                <p class="old"><?php next_posts_link(__( '&lt;&lt;', 'roots' )); ?></p>
                <div class="clearfix"></div>
            </footer>
        <?php endif; ?>



<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>
