/*	Author:
		TMW - (Author Name Here)
*/

// Create a closure to maintain scope of the '$' and KO (Kickoff)
;
(function(KO, $) {

    $(function() {
        // Any globals go here in CAPS (but avoid if possible)

        // follow a singleton pattern
        // (http://addyosmani.com/resources/essentialjsdesignpatterns/book/#singletonpatternjavascript)

        KO.Config.init();

        // var container = $('.blogTable');
        // var msnry = new Masonry(container, {
        //     // options
        //     // columnWidth: '.blogList-item',
        //     itemSelector: '.blogList-item'
        // });

        var $container = $('.blogTable');
        // initialize Masonry after all images have loaded
        $container.imagesLoaded( function() {
            $container.packery({
            // options
                columnWidth: '.grid-sizer',
                itemSelector: '.blogList-item',
                gutter: '.gutter-sizer'
            });
        });



        $(".show-bar").on('click', function() {
            $(".opener").slideToggle(200);
            $(".show-bar").toggleClass('show-bar--active');

            $(".show-bar").text(function(i, v) {
                return v === '—' ? '+' : '—'
            })

        });

        $(".search-icon").on('click', function() {

            $(".show-form").toggleClass('isOpened');
        });

    }); // END DOC READY


    /* Optional triggers

	// WINDOW.LOAD
	$(window).load(function() {

	});

	// WINDOW.RESIZE
	$(window).resize(function() {

	});

	*/



    KO.Config = {
        variableX: '', // please don't keep me - only for example syntax!

        init: function() {
            console.debug('Kickoff is running');
        }
    };

    // Example module
    /*
	KO.Ui = {
		init : function() {
			KO.Ui.modal();
		},

		modal : function() {

		}
	};
	*/

})(window.KO = window.KO || {}, jQuery);