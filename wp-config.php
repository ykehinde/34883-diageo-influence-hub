<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'influencehub');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_BN~=SNf~ $,37 X|2s]ecQ+biFDg0PArJzz*/XgoU0Vg-*<lx;A2+5;KI?-;cF(');
define('SECURE_AUTH_KEY',  '+e|$94H^@*rFfj3Y&h/s^!}abU4vOR%M0*k}u0+DsATde^#APmT0Mz*A2`Cf!~@T');
define('LOGGED_IN_KEY',    'sDxv^(ywG4/pXJiBg5X.EAS9<!z|P/)63mjPpW_]wr9_aSx61l_-yFF|cGI4KLdK');
define('NONCE_KEY',        '_Oc,Gi-CP=<R.GQrae@u-]ZO/s8G`(&_CC:)}>;H`pvo{TqZ]6${3dql|_b.~[Cw');
define('AUTH_SALT',        'FbavB*/gW|H$^Gf.>`zUif,|BOldQ],GR|=m].R5~Uuv=jV}EKeQDwn[ d1XYN %');
define('SECURE_AUTH_SALT', '( 2>J*Ep*xflRJbFfIg5yff {37jp9,&RQt`y?zkA(<acoC~E<|q&Mhfht]LT2mS');
define('LOGGED_IN_SALT',   'a5v@.v-]!j#.R}s=5sIA$Hvj5O86rlMC~@,u79:Ct 6|p?x|]L;j3]EPL+.{F7*5');
define('NONCE_SALT',       'BN6c/=Q43,!JY!hcEb7y;;s%|aJ?+cZ.gU^8hp^$-zsJ)!`=z2b^?W3%EY$sn4)Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
