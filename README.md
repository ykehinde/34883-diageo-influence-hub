# Diageo's influence hub #

Wordpress based blog built primarily for Diageo employees whilst being managed at tmw.

### Languages ###

* HTML
* CSS
* PHP

### How do I get set up? ###

* Setup using Kickoff and a wordpress structure template

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Developer - Yemi Kehinde
* Senior Developer - Zander Martineau
* Project Manager - Sara Heatherington