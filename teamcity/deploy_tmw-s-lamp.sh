# rsync code
echo "rsync -arivzt --stats --exclude-from=$1/build/deploy_tmw-s-lamp_exclude.txt $1/src/ -e \"ssh -i /home/teamcity/private.pem\" root@192.168.10.110:/var/www/diageo/theinfluencehub/www\""
rsync -arivzt --stats --exclude-from=$1/build/deploy_tmw-s-lamp_exclude.txt $1/src/ -e "ssh -i /home/teamcity/private.pem" root@192.168.10.110:/var/www/diageo/theinfluencehub/www

# setting directory permissions
echo "/usr/bin/ssh -i /home/teamcity/private.pem root@192.168.10.110 \"chown -R apache:apache /var/www/diageo/theinfluencehub/www\""
/usr/bin/ssh -i /home/teamcity/private.pem root@192.168.10.110 "chown -R apache:apache /var/www/diageo/theinfluencehub/www"
echo "/usr/bin/ssh -i /home/teamcity/private.pem root@192.168.10.110 \"find /var/www/diageo/theinfluencehub/www/ -type d -print0 | xargs -0 chmod 0755\""
/usr/bin/ssh -i /home/teamcity/private.pem root@192.168.10.110 "find /var/www/diageo/theinfluencehub/www/ -type d -print0 | xargs -0 chmod 0755"
echo "/usr/bin/ssh -i /home/teamcity/private.pem root@192.168.10.110 \"find /var/www/diageo/theinfluencehub/www/ -type f -print0 | xargs -0 chmod 0644\""
/usr/bin/ssh -i /home/teamcity/private.pem root@192.168.10.110 "find /var/www/diageo/theinfluencehub/www/ -type f -print0 | xargs -0 chmod 0644"
